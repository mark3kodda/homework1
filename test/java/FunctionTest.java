import main.java.org.Homework.models.Function;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FunctionTest {

    private Function cut = new Function();

    @Test
    void getNameOfDayTestMonday(){
        String expected = "Monday";
        String actual = cut.getNameOfDay(1);
        assertEquals(expected,actual,"must return String day of the week, when given number ");
    }

    @Test
    void getNameOfDayTestWednesday(){
        String expected = "Wednesday";
        String actual = cut.getNameOfDay(3);
        assertEquals(expected,actual,"must return String day of the week, when given number ");
    }

    @Test
    void checkDistanceBetweenTwoDotsTest(){
        double expected = 50;
        double actual = cut.checkDistanceBetweenTwoDots(0,0,50,0);
        assertEquals(expected,actual,"return distance between two dots in coordinates");
    }

    @Test
    void getNumberAsWordTest(){
        String expected = "девятьсот двенадцать";
        String actual = cut.getNumberAsWord(912);
        assertEquals(expected,actual,"return String value of given int number");
    }

    @Test
    void pickNumberFromStringHardTest(){
        long expected = 9012;
        long actual = cut.pickNumberFromStringHard("девять тысяч двенадцать");
        assertEquals(expected,actual,"return given int as String value");

    }

}