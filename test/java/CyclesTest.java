import main.java.org.Homework.models.Cycles;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CyclesTest {
    private Cycles cut = new Cycles();

    @Test
    public void sumAndCountEvenElements6()
    {
        int [] expected = new int[]{2450,50};
        int [] actual = Cycles.sumAndCountEvenElements6();
        assertArrayEquals(expected, actual );

    }

    @Test
    void getSumOfEvenNumbersTest(){
        String expected = "Sum of even numbers is (2450), amount of even numbers is (50)";
        String actual = cut.getSumAndCountOfEvenNumbers();
        assertEquals(expected,actual,"There we go");
    }


    @Test
    void mirroringNumberTest321() {
        int expected = 123;
        int actual = cut.mirroringNumber(321);
        assertEquals(expected,actual,"mirroringNumber method should mirror given number");
    }

    @Test
    void getSumOfDigitsInNumberTest81(){
        int expected = 9;
        int actual = cut.getSumOfDigitsInNumber(81);
        assertEquals(expected,actual);
    }
    @Test
    void getSumOfDigitsInNumberTest12458(){
        int expected = 20;
        int actual = cut.getSumOfDigitsInNumber(12458);
        assertEquals(expected,actual);
    }
    @Test
    void getFactorialTest(){
        int expected = 6;
        int actual = cut.getFactorial(3);
        assertEquals(expected,actual);
    }

    @Test
    void getSquareRootConsistentSelectionTest9(){
        int expected = 3;
        int actual = cut.getSquareRootConsistentSelection(9);
        assertEquals(expected,actual);
    }

    @Test
    void getSquareRootConsistentSelectionTest16(){
        int expected = 4;
        int actual = cut.getSquareRootConsistentSelection(16);
        assertEquals(expected,actual);
    }

    @Test
    void checkIfSimpleNumberTest(){
        String expected = "YES";
        String actual = cut.checkIfSimpleNumber(7);
        assertEquals(expected,actual);
    }

}

