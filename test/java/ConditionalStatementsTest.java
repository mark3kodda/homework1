import main.java.org.Homework.models.ConditionalStatements;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ConditionalStatementsTest {

    private ConditionalStatements cut = new ConditionalStatements();

    @Test
    void ErrorSample(){
        Assertions.assertThrows(NumberFormatException.class, () -> {
            Integer.parseInt("One");
        });
    }
    @Test
    void findQuarterByCoordinatesTestFirstQuarter()
    {
        int [] input = new int[]{1,2};
        String expected = "FIST quarter";
        String actual = cut.findQuarterByCoordinates(input);
        assertEquals(expected,actual);
    }
    @Test
    void findQuarterByCoordinatesTestFourthQuarter()
    {
        int [] input = new int[]{1,-1};
        String expected = "FOURTH quarter";
        String actual = cut.findQuarterByCoordinates(input);
        assertEquals(expected,actual);
    }
    @Test
    void findQuarterByCoordinatesTestSecondQuarter()
    {
        int [] input = new int[]{-1,1};
        String expected = "SECOND quarter";
        String actual = cut.findQuarterByCoordinates(input);
        assertEquals(expected,actual);
    }
    @Test
    void findQuarterByCoordinatesBetweenFirstAndSecond()
    {
        int [] input = new int[]{0,1};
        String expected = "between FIRST AND SECOND quarters";
        String actual = cut.findQuarterByCoordinates(input);
        assertEquals(expected,actual);
    }

    @Test
    void findQuarterByCoordinatesTestZeroCoordinate()
    {
        int [] input = new int []{0,0};
        String expected = "ZERO coordinate";
        String actual = cut.findQuarterByCoordinates(input);
        assertEquals(expected,actual);
    }

    @Test
    void checkIfEvenNumberTest8() {
        int expected = 8;
        int actual = cut.checkIfEvenNumber(4, 2);
        assertEquals(expected, actual, " if a%2=0 should a*b");
    }
    @Test
    void checkIfEvenNumberTest5(){
        int expected = 5;
        int actual = cut.checkIfEvenNumber(3,2);
        assertEquals(expected,actual, "if a%2!=0 should a+b ");
    }

    @Test
    void getSumOfPositiveNumbersTestAllPositives(){
        int expected = 3;
        int actual = cut.getSumOfPositiveNumbers(1,1,1);
        assertEquals(expected,actual);
    }
    @Test
    void getSumOfPositiveNumbersTestNonePositives(){
        int expected = 0;
        int actual = cut.getSumOfPositiveNumbers(-1,-5,-3);
        assertEquals(expected,actual);
    }
    @Test
    void getSumOfPositiveNumbersTestOnePositive(){
        int expected = 3;
        int actual = cut.getSumOfPositiveNumbers(-50,-1,3);
        assertEquals(expected,actual);
    }
    @Test
    void chooseMultiplyingOrAddingTest1(){
        int expected = 6;
        int actual = cut.chooseMultiplyingOrAdding(1,1,1);
        assertEquals(expected,actual);
    }
    @Test
    void chooseMultiplyingOrAddingTest2(){
        int expected = 11;
        int actual = cut.chooseMultiplyingOrAdding(2,2,2);
        assertEquals(expected,actual);
    }
    @Test
    void setStudentRateTestB() {
        String expected = "B";
        String actual = cut.setStudentRate(80);
        assertEquals(expected,actual);
    }
}