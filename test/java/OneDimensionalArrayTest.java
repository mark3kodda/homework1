import main.java.org.Homework.models.OneDimensionalArray;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class OneDimensionalArrayTest {

    private OneDimensionalArray cut = new OneDimensionalArray();

    @Test
    void findMinElementInArrayTest() {
        int [] toBeTested = new int[]{2,3,4,5,1};
        int expected = 1;
        int actual = cut.findMinElementInArray(toBeTested);
        assertEquals(expected,actual,"must find MIN element in array");
    }

    @Test
    void findMaxElementInArrayTest() {
        int [] toBeTested = new int[]{2,3,4,5,1};
        int expected = 5;
        int actual = cut.findMaxElementInArray(toBeTested);
        assertEquals(expected,actual,"must find MAX element in array");
    }

    @Test
    void findMinIndexInArrayTest(){
        int[] toBeTested = new int [] {2,3,4,5,1};
        int expected = 4;
        int actual = cut.findMinIndexInArray(toBeTested);
        assertEquals(expected,actual,"must show INDEX of MIN element in array");
    }
    @Test
    void maxElemIndexTest(){
        int [] toBeTested = new int [] {2,3,4,5,1};
        int expected = 5;
        int actual = cut.findMaxIndexInArray(toBeTested);
        assertEquals(expected,actual, "must show INDEX of MAX element in array");
    }

    @Test
    void findSumOfOddElements(){
        int [] toBeTested = new int [] {2,3,4,5,12};
        int expected = 8;
        int actual = cut.findSumOfOddElements(toBeTested);
        assertEquals(expected,actual,"must show SUM of ODD elements of array");
    }

    @Test
    void reverseArrayTest(){
        int [] toBeTested = new int [] {2,3,4,5,1};
        int [] expected = new int [] {1,5,4,3,2};
        int [] actual = cut.reverseArray(toBeTested);
        assertArrayEquals(expected, actual);
    }

    @Test
    void getNumberOfOddElementsTest(){
        int [] toBeTested = new int [] {2,3,4,5,1};
        int expected = 2;
        int actual = cut.getNumberOfOddElements(toBeTested);
        assertEquals(expected,actual,"must count number of ODD elements");
    }

    @Test
    void switchArrayFrontBackTest(){
        int [] toBeTested = new int [] {1,2,3,4,5,6,7,8};
        int [] expected = new int [] {5,6,7,8,1,2,3,4};
        int [] actual = cut.switchArrayFrontToBack(toBeTested);
        assertTrue(Arrays.equals(expected,actual));
    }

    @Test
    void SortArrayBubbleTest(){
        int [] toBeTested = new int [] {5,6,7,8,2,3,4,1};
        int [] expected = new int [] {1,2,3,4,5,6,7,8};
        int [] actual = cut.SortArrayBubble(toBeTested);
        assertTrue(Arrays.equals(expected,actual));
    }

    @Test
    void SortArrayInsertionTest(){
        int [] toBeTested = new int [] {32,123,111,101,56};
        int [] expected = new int [] {32, 56, 101,111,123};
        int [] actual = cut.SortArrayInsertion(toBeTested);
        assertTrue(Arrays.equals(expected,actual));
    }

}