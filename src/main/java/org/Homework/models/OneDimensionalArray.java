package main.java.org.Homework.models;

import java.util.Arrays;

public class OneDimensionalArray {

    // Найти минимальный элемент массива
    public int findMinElementInArray(int [] a){
        int res;
        int tempMin = a[0];
        for (int i = 0; i < a.length; i++) {
            if(tempMin > a[i]){
                tempMin= a[i];
            }
        }
        res = tempMin;
        return res;
    }

    // Найти максимальный элемент массива
    public int findMaxElementInArray(int [] a){
        int res;
        int tempMax = a[0];
        for (int i = 0; i < a .length ; i++) {
            if(tempMax < a[i]){
                tempMax = a[i];
            }
        }
        res = tempMax;
        return res;

    }

    // Найти индекс минимального элемента массива
    public int findMinIndexInArray(int [] a){
        int res = 0;
        int tempMin = a[0];
        int tempMinIndex = 0;
        for (int i = 0; i < a.length; i++) {
            if(tempMin > a[i]){
                tempMin= a[i];
                tempMinIndex = i;
            }
        }
        res = tempMinIndex;
        return res;
    }

    // Найти индекс максимального элемента массива
    public int findMaxIndexInArray(int [] a){
        int res;
        int tempMax = a[0];
        int tempMaxIndex = 0;
        for (int i = 0; i < a.length; i++) {
            if(tempMax < a[i]){
                tempMax= a[i];
                tempMaxIndex = i;
            }
        }
        res = tempMaxIndex;
        return res;
    }

    // Посчитать сумму элементов массива с нечетными индексами
    public int findSumOfOddElements( int [] a ){
        int res = 0;
        for (int i = 0; i <a.length ; i++) {
            if(i%2!=0){
                res = res+a[i];
            }
        }
        return res;
    }

    // Сделать реверс массива (массив в обратном направлении)
    public int [] reverseArray(int [] a ){
        int [] res = new int[a.length];
        int counter = 0;

        for (int i = a.length-1; i >=0 ; i--) {
            res[counter] = a[i];
            counter++;
        }
        return res;
    }

    // Посчитать количество нечетных элементов массива
    public int getNumberOfOddElements(int [] a){
        int res = 0;
        for (int i = 0; i <a.length ; i++) {
            if(i%2!=0){
                res++;
            }
        }
        return res;
    }

    // Поменять местами первую и вторую половину массива, например, для массива
    // 1 2 3 4, результат 3 4 1 2
    // какая реакция должна быть если массив не делиться ровно на 2??
    public int [] switchArrayFrontToBack(int [] a){
        int [] res = new int [a.length];
        for (int i = 0; i <a.length/2 ; i++) {
            res[i+a.length/2] = a[i];
        }
        for (int i = 0; i <a.length/2 ; i++) {
            res[i]= a[a.length/2+i];
        }
        System.out.println(Arrays.toString(res));
        return res;
    }

    //Отсортировать массив (пузырьком (Bubble), выбором (Select), вставками (Insert))
    public int [] SortArrayBubble(int [] a){

        boolean isSorted = false;
        int buf = 0;
        while(!isSorted){
            isSorted = true;
            for (int i = 0; i <a.length-1 ; i++) {
                if(a[i] > a[i+1]){
                    isSorted = false;
                    buf=a[i];
                    a[i] = a[i+1];
                    a[i+1] = buf;
                }
            }
        }
        return a;
    }

    //сортировка выбором Select
    public int [] SortArraySelect(int [] a){

        for (int i = 0; i <a.length ; i++)
        {
            int pos = i;
            int min = a[i];
            for (int j = i + 1; j <a.length ; j++) {
                if(a[j] < min ){
                    pos = j;
                    min = a[j];
                }
            }
            a[pos] = a[i];
            a[i] = min;
        }
        return a;
    }

    public int [] SortArrayInsertion(int [] a ){

        for (int left = 0; left <a.length ; left++) {

            int value = a[left];
            int i = left-1;
            for (; i >= 0 ; i--) {
                if(value< a[i]){
                    a[i+1] = a[i];
                }else{
                    break;
                }
                }
            a[i+1]=value;
        }
        return a;
    }

}
