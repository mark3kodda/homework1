package main.java.org.Homework.models;

public class ConditionalStatements {

    // Если а – четное посчитать а*б, иначе а+б
    public int checkIfEvenNumber(int a, int b){
        int res;
        if(a%2==0){
            res = a*b;
        }else{
            res = a+b;
        }
        return  res;
    }

    // Определить какой четверти принадлежит точка с координатами (х,у)
    public String findQuarterByCoordinates(int [] XYcoordiantes){
        String res = null;
        int x = XYcoordiantes[0];
        int y = XYcoordiantes[1];
        if( x > 0 & y > 0){
            res = "FIST quarter";
        }else if ( x < 0 & y > 0 ){
            res = "SECOND quarter";
        }else if ( x > 0 & y < 0 ){
            res = "FOURTH quarter";
        }else if ( x < 0 & y < 0 ){
            res = "THIRD quarter";
        }else if ( x ==0 & y==0 ){
            res = "ZERO coordinate";
        }else if ( x == 0 & y > 0 ){
            res = "between FIRST AND SECOND quarters";
        }else if ( x == 0 & y < 0){
            res = "between FOURTH AND THIRD quarters";
        }else if ( y == 0 & x > 0 ){
            res = "between FOURTH AND FIRST quarters";
        }else if ( x < 0 & y == 0){
            res = "between SECOND AND THIRD quarters";
        }

        return res;
    }

    // Найти суммы только положительных из трех чисел
    public int getSumOfPositiveNumbers(int a, int b, int c){
        int res = 0;
        if (a>0){
            res = res+a;
        }
        if (b>0){
            res = res+b;
        }
        if (c>0){
            res = res+c;
        }
        return res;
    }

    // Посчитать выражение (макс(а*б*с, а+б+с))+3
    public int chooseMultiplyingOrAdding(int a, int b, int c){
        int res = 0;

        if (a*b*c>a+b+c){
            res = a*b*c+3;
        }else if (a+b+c>a*b*c){
            res=a+b+c+3;
        }else if (a+b+c == a*b*c){
            res = 0;
            System.out.println("we have even numbers");
        }
        return res;
    }

    // Написать программу определения оценки студента по его рейтингу
    public String setStudentRate(int a){
        String res = null;

        //0-19 20-39 40-59 60-74 75-89 90-100
        //  F    E     D     C     B     A

        if(0 <=a & a<=19){
            res = "F";
        }else if(21 <= a & a <= 39){
            res = "E";
        }else if(40 <= a & a <= 59){
            res = "D";
        }else if (60 <= a & a <= 74){
            res = "C";
        }else if (75 <= a & a <= 89){
            res = "B";
        }else if (90 <= a & a <= 100){
            res = "A";
        }else if (a>101 || a<-1 ){
            res = "Inadequate rate";
        }
        return res;
    }
}
