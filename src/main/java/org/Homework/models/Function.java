package main.java.org.Homework.models;

public class Function {

    public String getNameOfDay(int a ){
        String res = "";
        int day = a;
        switch (day){
            case 1:
                res = "Monday";
                break;
            case 2:
                res = "Tuesday";
                break;
            case 3:
                res = "Wednesday";
                break;
            case 4:
                res = "Thursday";
                break;
            case 5:
                res = "Friday";
                break;
            case 6:
                res = "Sunday";
                break;
            case 7:
                res = "Saturday";
                break;
        }

        return res;
    }

    public double checkDistanceBetweenTwoDots(double xa, int ya, int xb, int yb ){
        double res;
        res = Math.sqrt((getSquared(xb-xa))+(getSquared(yb-ya)));
        return res;
    }

    //возведение в квадрат *(собсветнный)
    private double getSquared (double a){
        double res = a*a;
        return res;
    }

    //Вводим число(0-999), получаем строку с прописью числа
    //создать массив единицы / десятки / сотни /

    public String getNumberAsWord(int a){
        String res;
        String number =  String.valueOf(a);
        String temp = null;
        //---------------------------------------------

        // уникальные слова 0.11.12.13.14.15.16.17.18.19
        String [][] teens = new String[][]{
                {"","одинадцать","двенадцать","тринадцать","четырнадцать","пятнадцать","шестнадцать","семьнадцать", "восемьнадцать", "девятнадцать"},
        };

        String [][] vocabulary = new String[][]{
                {"","сто","двести","триста","четыреста","пятьсот","шестьсот", "семьсот", "восемьсот", "девятьсот"},
                {"","десять","двадцать","тридцать","сорок","пятьдесят","шесьдесят", "семьдесят", "восемьдесят", "девяносто"},
                {"","один","два","три","четыре","пять", "шесть", "семь", "восемь", "девять"}
        };
        //в зависимости от длинны цифры, можно переписать цикл чтоб начинал с нужного ряда словаря и шёл куда нам надо
        //используем цикл, где ЦЫФРЫ из номера и НОМЕР цикла *(выбирает этаж) пример ниже
        //temp = vocabulary [0][2] + " " + vocabulary [1][5]+ " " + vocabulary [2][1]; //ПРИМЕР СБОРА СТРОКИ
        //---------------------------------------------
        //работает если число 1, но ЕСЛИ число будет 0017, 009
        if(number.length()==1){
            if(number.charAt(0)=='0'){
                temp = "нуль";
            }
            else{
                temp = vocabulary[2][a];
            }
            //теперь надо взять цифру из переданных
            //раз мы знаем что цифра одна, мы можем как раз её и взять
        }

        // работа для 2ух чисел - десятки, но ЕСЛИ число будет 0017, 0095
        if(number.length()==2)
        {
            if(number.charAt(0)=='1')
            {
                temp = teens[0][pickValueFromCharAtIndex(number,1)];
            }
            else if(number.charAt(0)!=1)
            {
                temp = vocabulary[1][pickValueFromCharAtIndex(number,0)]+" "+vocabulary[2][pickValueFromCharAtIndex(number,1)];
            }
        }

        //работа для 3ёх чисел
        if(number.length()==3)
        {
            //разбей тут темп на 2 части, сотня + сдача
            temp = vocabulary[0][pickValueFromCharAtIndex(number,0)]; // это сотни
            if(number.charAt(1)=='1'){
                temp = temp + " " + teens[0][pickValueFromCharAtIndex(number,2)];
            }else if(number.charAt(0)!=1){
                temp = temp + " " + vocabulary[1][pickValueFromCharAtIndex(number,1)]+" "+vocabulary[2][pickValueFromCharAtIndex(number,2)];
            }
        }

        String secondTemp = null;
        String [] bufferRes = null;
        bufferRes = temp.split(" ");

        //вытягиваем слова из массива результатов расставляя правильно пробелы
        if(bufferRes.length == 1){
            secondTemp=bufferRes[0];
        }else if(bufferRes.length == 2){
            secondTemp=bufferRes[0];
            secondTemp=secondTemp+" "+bufferRes[1];
        }else if(bufferRes.length == 3){
            secondTemp=bufferRes[0];
            secondTemp=secondTemp+" "+bufferRes[1]+" "+ bufferRes[2];
        }
        res = secondTemp;
        return res;
    }

    private int pickValueFromCharAtIndex(String word, int neededIndex ){
        int i = Integer.parseInt(String.valueOf(word.charAt(neededIndex)));
        return i;
    }

    public long pickNumberFromStringHard(String a ) throws IllegalArgumentException{

        long res;
        long temp = 0;

        String [] words = a.split(" "); // мы загоняем данный Стринг в массив с помощью разделителя пробела

        String [][] asWords = new String[][] {
                //значения
                {"нуль","один","одна","две","два","три","четыре","пять","шесть","семь","восемь","девять"},
                {"десять","одинадцать","двенадцать","тринадцать","четырнадцать","пятнадцать","шестнадцать","семнадцать","восемнадцать","девятнадцать"},
                {"двадцать","тридцать","сорок","пятьдесят","шестьдесят","семьдесят","восемьдесят","девяносто"},
                {"сто","двести","триста","четыреста","пятьсот","шестьсот","семьсот","восемьсот","девятьсот"},
                //степени
                {"тысяч", "тысячи", "тысяча"},
                {"миллионов", "миллиона", "миллион"},
                {"миллиардов", "миллиарда", "миллиард"}
        };
        int [] floorCaseComparison = new int[words.length];                            // массив, индексов нашего СТРИНГА сверяя по словарю asWords
        String [] wordsOfPower = new String[words.length];

        for(int i = 0; i < words.length; i++){
            for (int j = 0; j <asWords.length ; j++) {                                 // вход в словарь
                for (int k = 0; k <asWords[j].length ; k++) {                          // вход в этаж
                    if(words[i].equals(asWords[j][k])){
                        floorCaseComparison[i] = j;                                    // мы создаём массив из индексов ЭТАЖЕЙ, всех слов в целевом СТРИНГЕ/массиве *(если будут соседние повторения то invalid input)
                        wordsOfPower[i]=asWords[j][k];
                    }
                }
            }
        }
        //если в строке есть слово НУЛЬ+что-то вали весь СТРИНГ *(null не 0, разница есть)
        //можно проверить сам массив words и если есть
        for (int i = 0; i <words.length ; i++) {
            if(words[i].equals("нуль")){
                if(words.length>1){
                    throw new IllegalArgumentException("Invalid input");
                }
            }
        }
        //проверяем повторение кратностей тысяч тысячей, на неком расстоянии    *(двенадцать тысяч три тысячи)
        //нам нужно повторение конкретных цыфр этажей 4.5.6

        if (checkIfThereIsAPair(4,floorCaseComparison)>0){
            throw new IllegalArgumentException("Invalid input - double thousand");
        }
        if (checkIfThereIsAPair(5,floorCaseComparison)>0){
            throw new IllegalArgumentException("Invalid input - double million");
        }
        if (checkIfThereIsAPair(6,floorCaseComparison)>0){
            throw new IllegalArgumentException("Invalid input - double billion");
        }

        //можно проверить последовательность озвучивания МИЛЛИАРД. МИЛЛИОН. ТЫСЯЧА
        //если у нас встречаются  все 6.5.4. то стоять они должны в таком то порядке иначе инвалид
        //следующее надо проверять ЛИШЬ если есть все 3 этажа кратностей
        if(checkIfThereIsTargetNumber(6,floorCaseComparison) == 0 & checkIfThereIsTargetNumber(6,floorCaseComparison) == checkIfThereIsTargetNumber(5,floorCaseComparison)
        & checkIfThereIsTargetNumber(6,floorCaseComparison) == checkIfThereIsTargetNumber(4,floorCaseComparison)){
            if(checkIfABiggerThanB(6,5,floorCaseComparison)==checkIfABiggerThanB(5,4,floorCaseComparison)){

            }else{
                throw new IllegalArgumentException("Invalid input 6.5.4. order case");
            }
        }
        //ниже проверки последовательности появлений пар слов "миллиард - миллион / миллиард - тысяча / миллион - тысяча"
        if(checkIfThereIsTargetNumber(5,floorCaseComparison) == 0 & checkIfThereIsTargetNumber(5,floorCaseComparison) == checkIfThereIsTargetNumber(4,floorCaseComparison) ){
            if(checkIfABiggerThanB(5,4,floorCaseComparison)>0){
                throw new IllegalArgumentException("Invalid input thousand million");
            }
        }
        if(checkIfThereIsTargetNumber(6,floorCaseComparison) == 0 & checkIfThereIsTargetNumber(6,floorCaseComparison) == checkIfThereIsTargetNumber(5,floorCaseComparison)){
            if(checkIfABiggerThanB(6,5,floorCaseComparison)>0){
                throw new IllegalArgumentException("Invalid input million billion");
            }
        }

        if(checkIfThereIsTargetNumber(6,floorCaseComparison) == 0 & checkIfThereIsTargetNumber(6,floorCaseComparison) == checkIfThereIsTargetNumber(4, floorCaseComparison)){
            if(checkIfABiggerThanB(6,4,floorCaseComparison)>0){
                throw new IllegalArgumentException("Invalid input thousand billion");
            }
        }
        //нужна проверка на слово ОТСУТСТВУЮЩЕЕ В СЛОВАРЕ
        //может происходить сравнением длин массивов words и floorCaseComparison
        for (int i = 0; i <floorCaseComparison.length ; i++) {
            if(!words[i].equals(wordsOfPower[i])){
                throw new IllegalArgumentException("There is an INVALID WORD - " + words[i]);
            }

        }
        if(words.length>floorCaseComparison.length){
            throw new IllegalArgumentException("There is an INVALID WORD");
        }
        //проверяем случай "тринадцать три"
        for (int i = 0; i <floorCaseComparison.length-1 ; i++) {
            if(floorCaseComparison[i]==1 && floorCaseComparison[i+1]==0){
                throw new IllegalArgumentException("Invalid input syntax error");
            }
        }
        //мы можем проверить индексы и по ним найти сколько кратностей встречается у нас в стринге, и сразу передать это в multiplicity чтоб потом это же число допустить "прыжки вверх"
        int multiplicity = 0;
        for (int i = 0; i <floorCaseComparison.length ; i++) {
            if(floorCaseComparison[i]>3){
                multiplicity++;
            }
        }
        //теперь мы можем прыгнуть вверх столько раз, сколько нам позволяет multiplicity
        for (int i = 0; i <floorCaseComparison.length-1 ; i++) {
            if(floorCaseComparison[i]>floorCaseComparison[i+1]){                       // если наши числа идут в уменьшение И есть запас multiplicity
            }else{                                                                     // если ход прыгает вверх по значению multiplicity уменьшается
                multiplicity--;
                //System.out.println("multiplicity downgrade, because of (" + floorCaseComparison[i]+" and "+floorCaseComparison[i+1]+")");
            }
            if (multiplicity<=-1){
                throw new IllegalArgumentException("Invalid input out of multiplicity bound");
            }
        }
                                                                       // значения по кратности могут идти ЛИШЬ вниз ЛИБО прыгать на кратность
                                                      // один миллион двенадцать тысяч шестьдесят восемь               и они не могут прыгать ДВАДЖЫ на одну и ту же кратность
                                           //надо отследить движение по кратностям, а НЕ совпадения, движение должно идти сверху вниз ИЛИ прыгать на кратности
        //----
        // выше происходит проверка на правильность ввода
        // нижнее должно происходить лишь при правильном вводе

        int [][] asValue = new int[][]
                {
                        //значения
                        {0, 1, 1, 2, 2, 3, 4, 5, 6, 7, 8, 9},
                        {10, 11, 12, 13, 14, 15, 16, 17, 18, 19},
                        {20, 30, 40, 50, 60, 70, 80, 90},
                        {100, 200, 300, 400, 500, 600, 700, 800, 900},
                        //степени
                        {1000, 1000, 1000},
                        {1000000, 1000000, 1000000},
                        {1000000000, 1000000000, 1000000000}
                };

        long [] tempResult = new long [asWords.length];// последние NULL места ничего к сумме не прибавят
        int tempResCounter = 0;
        //теперь делаем подбор сопадения слов  --- один миллион двенадцать тысяч пядесят восемь
        for (int i = 0; i <words.length ; i++)                      // перебор слов в целевом массиве
        {
            for (int j = 0; j <asWords.length ; j++)                //вход в словарь
            {
                for (int k = 0; k <asWords[j].length ; k++)         //вход в этаж
                {
                    if(words[i].equals(asWords[j][k])){             //отметка СОВПАДЕНИЯ
                        if( j < 4){
                            temp = temp + asValue[j][k];
                        }
                        else if(temp == 0)
                        {
                            temp = temp+asValue[j][k];
                            tempResult[tempResCounter]=temp;                   //записали результат в таблицу и обнулили
                            temp=0;
                            tempResCounter++;
                        }
                        else {
                                                                                //умножить на пердидущее число //  после возведения в степень можно загонять результат в массив int по финалу всё плюсовать (100 миллионов 64 тысячи)
                            temp = temp*(asValue[j][k]);                        // возведение предидущего числа в степень и плюсование к массе
                                                                                // int java range -2,147,483,648

                            System.out.println("");
                            tempResult[tempResCounter]=temp;                  //записали результат в таблицу и обнулили
                            temp=0;
                            tempResCounter++;
                        }
                    }
                }
            }
        }
        long wholeTempRes= 0;
        for (int i = 0; i <tempResult.length ; i++)
        {
            wholeTempRes=wholeTempRes + tempResult[i];
        }
        temp = temp + wholeTempRes;
        res = temp;
        System.out.println(String.format("%,d", res));
        return res;
    }

    private int checkIfThereIsTargetNumber(int targetNumber, int [] array){
        int res = 1;
        for (int i = 0; i <array.length ; i++) {
            if(array[i]==targetNumber){
                res = 0;
                return res;
            }

        }
        return res;
    }

    private int checkIfThereIsAPair(int targetNumber, int[] array){
        int res = 0;
        int temp = 0;
        for (int i = 0; i <array.length ; i++) {
            if (array[i] == targetNumber){
                temp++;
            }
        }
        if(temp>1){
            res = 1;
        }
        return res;
    }

    private int checkIfABiggerThanB(int firstNumber, int secondNumber,int [] array){
        //верёнт 0 если правильно стоят, вернёт 1 если НЕ правильно стоят - чего это не булеан??
        int res = 0;
        int indexOfFirstNumber = 0;
        int indexOfSecondNumber = 0;
        for (int i = 0; i <array.length ; i++) {
            if(array[i] == firstNumber){
                indexOfFirstNumber = i;
            }
        }
        for (int i = 0; i <array.length ; i++) {
            if(array[i] == secondNumber){
                indexOfSecondNumber = i;
            }
        }
        if(indexOfFirstNumber>indexOfSecondNumber){
            res = 1;
            return res;
        }
        return res;
    }
}