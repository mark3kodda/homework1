package main.java.org.Homework.models;

public class Cycles {

    public static int[] sumAndCountEvenElements6()
    {
        int sum = 0, count = 0;
        for (int i = 0; i < 100; i++) {
            if (i % 2 == 0){
                sum += i;
                count++;}
        }
        return new int[] {sum, count};
    }


    // Найти сумму четных чисел и их количество в диапазоне от 1 до 99
    public String getSumAndCountOfEvenNumbers(){

        int ressum = 0;
        int resamount = 0;
        String res;
        for (int i = 0; i <100 ; i++) {
            if(i%2==0){
                ressum = ressum+i;
                resamount++;
            }
        }
        res = "Sum of even numbers is ("+ressum+"), amount of even numbers is ("+resamount+")";
        return res;
    }

    // Проверить простое ли число?
    public String checkIfSimpleNumber(int a){
        String res;

        if(a == 2 | a == 3 | a ==5 | a==7){
            res = "YES";
        }else if (a%2!=0 & a%3!=0 & a%5!=0 & a%7!=0){
            res = "SecondYES";
        }else{
            res= "NO";
        }

        return res;
    }

    // Найти корень натурального числа с точностью до целого (рассмотреть вариант последовательного подбора )
    public int getSquareRootConsistentSelection(int a){
        int res = 0;

        while (res*res<a){
            res++;
            if(res*res>a){
                res= res -1;
                break;
            }
        }
        return res ;
    }
    //и метод бинарного поиска

    //Вычислить факториал числа n. n! = 1*2*…*n-1*n;
    public int getFactorial(int a){
        int res = 1;

        for (int i = 1; i <=a ; i++) {
            res = res * i;
        }
        return res;
    }

    //Посчитать сумму цифр заданного числа
    public int getSumOfDigitsInNumber(int a){
        int res = 0;
        String number = String.valueOf(a);
        for (int i = 0; i <number.length() ; i++) {
            char currentDigit = number.charAt(i);
            res = res + Integer.parseInt(String.valueOf(currentDigit));
        }
        return res;
    }

    //Вывести число, которое является зеркальным отображением последовательности цифр заданного числа, например, задано число 123, вывести 321.
    public int mirroringNumber(int a){

        int res;
        String number = String.valueOf(a);
        String numberReversed = "";
        for (int i = number.length(); i > 0 ; i--) {
            numberReversed = numberReversed + number.charAt(i-1);
        }
        res = Integer.parseInt(numberReversed);
        return res;
    }




}
